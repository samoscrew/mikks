<?php
include 'connection.php';
include "MPDF57/mpdf.php";
try{
	$db=connection::getConnectionInstance();
	$method = $_SERVER['REQUEST_METHOD'];
	if($method==='POST')
	{
		$html="";
		if(isset($_POST['html']))
			$html=$_POST['html'];
		//print_r($html);die;
		$year=date('Y');
    	$tmp="<style>
		table.my_table {
		font-family: verdana,arial,sans-serif;
		color:black;
		border: 1px solid black;
		border-collapse: collapse;
		font-size: 12px;
		width: 100%;
		box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
		display: table;
		}

		.my_table tr {
		display: table-row;
		background: #f6f6f6;
		border: 1px solid black;
		
		}

		.my_table tr:hover {
		display: table-row;
		background: #2980b9 !important;
		color:white;
		}

		tr:nth-of-type(odd) {
		background: #e9e9e9;
		}

		.my_table tr.header {
		font-family: verdana,arial,sans-serif;
		font-size: 12px;
		font-weight: bold;
		color: black;
		background: white !important;
		border: 1px solid black;

		}
	
		.my_table td {
		padding: 4px 7px;
		display: table-cell;
		border: 2px solid black;
		}
		</style>
    	<br/><b>Универзитет у Београду - Математички факултет <br/>
		Коначна ранг листа кандидата у уписном року Јун ".$year." </b><br/>
		<p>Свим кандидатима који су конкурисали на афирмативној ранг листи
		одобрен је упис преко афирмативне ранг листе.</p>
		<p>Упис ће бити огранизован према распореду објављеном на сајту факултета
		(upis.matf.bg.ac.rs)</p><br/>
		".$html;
    	$mpdf = new mPDF('utf-8' , 'A4' , '' , '' ,15,10,16,10,10,10); 
   		$mpdf->SetDisplayMode('fullpage');
     	$mpdf->SetHeader('Математички факултет');
		$mpdf->setFooter('{PAGENO}');
		$mpdf->debug=true;
		$mpdf->WriteHTML("$tmp");
		$mpdf->Output('Rang_lista_'.$year.".pdf",'F');
		echo json_encode('server/Rang_lista_'.$year.".pdf");
	}
	}
	catch(Exeption $e){}
?>