<?php
include 'connection.php';
function check_answer($correct, $mine){

	if($correct==$mine)
		return 3;
	else if ($mine=='N')
		return 0;
	else if ($mine=='X')
		return -1;
	else
		return -0.3;
}


$result = new stdClass();
try{

    $db=connection::getConnectionInstance();
	
	$method = $_SERVER['REQUEST_METHOD'];

     if($method==='POST')
	{
		$result->message = "Primljen zahtev za unos objekta";
		$application_id="";
		$exam_id="";
		$answers="";
		$path="";
		if(isset($_POST['application_id']))
			$application_id=$_POST['application_id'];
		if(isset($_POST['exam_id']))
			$exam_id=$_POST['exam_id'];
		if(isset($_POST['answers']))
			$answers=$_POST['answers'];
		if(isset($_POST['file_path']))
			$path=$_POST['file_path'];
		if($answers==""){
			$result->message="Podatak o odgovorima nije primljen";
			$result->error=true;
			$result->path=$path;
			echo json_encode($result);
			exit();
		}
		if($exam_id==""){
			$result->message="Podatak o ispitu nije primljen";
			$result->error=true;
			$result->path=$path;
			echo json_encode($result);
			exit();
		}
		if($application_id==""){
			$result->message="Podatak o prijavi nije primljen";
			$result->error=true;
			$result->path=$path;
			echo json_encode($result);
			exit();
		}

		$query = $db->prepare("SELECT correct_results FROM combinations WHERE exam_id={$exam_id}");
		$query->execute(); 
		$res = $query->fetchAll(PDO::FETCH_ASSOC);
		if(count($res)>0)
			$correct=$res[0]['correct_results'];
		else
			{
				$result->message="Trazeni test '{$exam_id}' ne postoji u bazi";
				$result->error=true;
				$result->path=$path;
				echo json_encode($result);
				exit();
			}
		$query = $db->prepare("SELECT * FROM candidates WHERE application_id='{$application_id}'");
		$query->execute(); 
		$res = $query->fetchAll(PDO::FETCH_ASSOC);
		if(count($res)==0){
			$result->message="Trazeni kandidat '{$application_id}' ne postoji u bazi";
			$result->error=true;
			$result->path=$path;
			echo json_encode($result);
			exit();
		}
		$query = $db->prepare("SELECT * FROM results WHERE application_id='{$application_id}' and exam_id={$exam_id}");
		$query->execute(); 
		$res = $query->fetchAll(PDO::FETCH_ASSOC);
		if(count($res)>0){
			$result->error_status = false; 
			$result->message="Test kandidata '{$application_id}' je uspesno pregledan";
			$result->path=$path;
			echo json_encode($result);
			exit();
		}

		$poeni=0;
		$correct_ex=str_split($correct);
		$mine_ex=str_split($answers);

		foreach ($correct_ex as $key => $value) {
			$poeni+=check_answer($value,$mine_ex[$key]);
		}

		
		$query = $db->prepare("INSERT INTO `entry_exam`.`results` (
								`application_id` ,
								`exam_id` ,
								`circled_results` ,
								`points_earned`
							)
							VALUES (
							'{$application_id}', '{$exam_id}', '{$answers}', {$poeni}
							);");
		$query->execute();  
		$num = $query->rowCount();
		if($num==1) {$result->error_status = false; $result->message="Test kandidata '{$application_id}' je uspesno pregledan"; $result->path=$path;}
		else {$result->error = true;$result->message="Podaci o ispitu se nisu sacuvali"; $result->path=$path;}
		
	}


}
catch(Exception $e){

    $result->error=true;
    $result->message = $e->getMessage();
    if(isset($_POST['file_path']))
    	$result->path=$_POST['file_path'];
}

echo json_encode($result);

?>
