<?php
include 'connection.php';
	try{
		$db=connection::getConnectionInstance();
		$method = $_SERVER['REQUEST_METHOD'];
		$html="";
		if($method==='GET')
		{
		$query = $db->prepare("	SELECT t1.application_id, concat( t1.last_name, ' , ', t1.first_name, ' ( ', t1.parent_name, ' ) ' ) AS full_name,t1.highschool_score, t2.points_earned, ROUND( t2.points_earned + t1.highschool_score, 2 ) AS total
								FROM candidates t1
								JOIN results t2 ON t1.application_id = t2.application_id
								ORDER BY 5 DESC ;");
		$query->execute(); 
		$res = $query->fetchAll(PDO::FETCH_ASSOC);
		
    	$year=date("Y");
		$html.='<table id="table1" class="my_table">';
    	
    	$html.="<tr class='header'>";
    	$html.='<td> Ранг</td>';
        $html.='<td> Број пријаве</td>';
        $html.='<td> Презиме, име (име родитеља)</td>';
        $html.='<td> Поени из средње школе</td>';
        $html.='<td> Пријемни испит</td>'; 
        $html.='<td> Укупно</td>';
    	$html.='</tr>';
		foreach ($res as $key => $value) {
			$html.='<tr>';
			$html.='<td>'.($key+1).'. </td>';
        	$html.='<td>'.$value['application_id'].'</td>';
        	$html.='<td>'.$value['full_name'].'</td>';
        	$html.='<td style="text-align:center">'.$value['highschool_score'].'</td>';
        	$html.='<td style="text-align:center">'.$value['points_earned'].'</td>'; 
        	$html.='<td style="text-align:center">'.$value['total'].'</td>';
    		$html.='</tr>';
		}
		$html.='</table>';
		echo $html;
		}
	}
	catch(Exception $e){
	}
	
?>
