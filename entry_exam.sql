-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2015 at 02:29 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `entry_exam`
--
CREATE DATABASE IF NOT EXISTS `entry_exam` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `entry_exam`;

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

DROP TABLE IF EXISTS `candidates`;
CREATE TABLE IF NOT EXISTS `candidates` (
  `application_id` varchar(5) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `jmbg` varchar(15) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `highschool_score` float NOT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`application_id`, `first_name`, `last_name`, `jmbg`, `parent_name`, `highschool_score`) VALUES
('123', 'Lola', 'Lolic', '1234567891234', 'Lolator', 20.8),
('202', 'Mirjana', 'Kostic', '2147483647', 'Slobodan', 38.5);

-- --------------------------------------------------------

--
-- Table structure for table `combinations`
--

DROP TABLE IF EXISTS `combinations`;
CREATE TABLE IF NOT EXISTS `combinations` (
  `exam_id` varchar(10) NOT NULL DEFAULT '',
  `correct_results` varchar(255) NOT NULL,
  PRIMARY KEY (`exam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `combinations`
--

INSERT INTO `combinations` (`exam_id`, `correct_results`) VALUES
('12345', 'AADCCEDABCDABABCDEAA');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
CREATE TABLE IF NOT EXISTS `results` (
  `application_id` varchar(5) NOT NULL,
  `exam_id` varchar(10) NOT NULL,
  `circled_results` varchar(255) NOT NULL,
  `points_earned` float NOT NULL,
  PRIMARY KEY (`application_id`),
  KEY ` exam_id` (`exam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`application_id`, `exam_id`, `circled_results`, `points_earned`) VALUES
('123', '12345', 'AANCCEXABCDAAABCDEAA', 2.1),
('202', '12345', 'AADCCEDABCDABABCDEAA', 2.1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `results`
--
ALTER TABLE `results`
  ADD CONSTRAINT `application_id_key` FOREIGN KEY (`application_id`) REFERENCES `candidates` (`application_id`),
  ADD CONSTRAINT `exam_id_key` FOREIGN KEY (`exam_id`) REFERENCES `combinations` (`exam_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
