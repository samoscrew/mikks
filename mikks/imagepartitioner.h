#ifndef IMAGEPARTITIONER_H
#define IMAGEPARTITIONER_H

#include "configuration.h"

#include <QObject>
#include <QString>

#include <vector>
#include <string>

#include <opencv2/core/core.hpp>

class ImagePartitioner : public QObject
{
  Q_OBJECT
public:
  explicit ImagePartitioner(const QString& pathToImg, bool filter = true, QObject *parent = 0);

  inline const std::vector<cv::Mat>& digitImages() const { return digits_; }

  inline const cv::Mat& circlesImage() const { return circlesImg_; }

  inline double scaleCoef() const { return scale_; }

  inline int yStartDiff() const { return dy_; }

  inline const cv::Point2f& referencePoint() const { return topLeftCorner_; }

  bool writeDigits(const QString& baseDir, unsigned startIdx) const;
private:
#ifdef DEBUG
  void displayImage(const cv::Mat& image, bool writeImg = false, const std::string &name = std::string()) const;
  void drawRotatedRect(cv::Mat& img, const cv::RotatedRect& header) const;
#endif

  cv::RotatedRect findHeaderRect(cv::Mat &fullImg) const;

  cv::Mat straightenScan(const cv::Mat &fullImg, const cv::RotatedRect& headerRect);

  void initCirclesImg(const cv::Mat &fullImg, const cv::Point2f& topLeftCorner);

  void initApplicationIdDigits(const cv::Mat& fullImg, const cv::Point2f &topLeftCorner);

  void initExamIdDigits(const cv::Mat& fullImg, const cv::Point2f &topLeftCorner);

  void initJMBGDigits(const cv::Mat& fullImg, const cv::Point2f &topLeftCorner);

  void prepareDigits();

  void cleanDigit(cv::Mat &img);

  void cleanDigits();

  void averagePixels(cv:: Mat& img);

  void averageImgs();

  double scale_;
  int dy_;
  cv::Point2f topLeftCorner_;
  cv::Mat circlesImg_;
  std::vector<cv::Mat> digits_;
};

#endif // IMAGEPARTITIONER_H
