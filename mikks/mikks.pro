QT += core gui network widgets
INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d \
        -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml \
        -lopencv_imgproc -lopencv_flann -lopencv_core -lopencv_hal
DISTFILES += \
    README.txt \
    templates/template300_1.jpg \
    templates/template600_1.jpg \
    templates/template300_10.jpg \
    templates/template600_10.jpg \
    classifiers/cascade.xml \
    classifiers/classifier.xml

SOURCES += \
    main.cpp \
    window.cpp \
    circledetector.cpp \
    matfformconstants.cpp \
    examsolution.cpp \
    numextractor.cpp \
    imagepartitioner.cpp

HEADERS += \
    window.h \
    circledetector.h \
    configuration.h \
    matfformconstants.h \
    examsolution.h \
    numextractor.h \
    imagepartitioner.h

QMAKE_CXXFLAGS += -std=c++11

QMAKE_CFLAGS_DEBUG += -DNDEBUG
