#include "numextractor.h"

void NumExtractor::rgb2cmyk(cv::Mat& img, std::vector<cv::Mat>& cmyk) {
    // Allocate cmyk to store 4 componets
    for (int i = 0; i < 4; i++) {
        cmyk.push_back(cv::Mat(img.size(), CV_8UC1));
    }

    // Get rgb
    std::vector<cv::Mat> rgb;
    cv::split(img, rgb);

    // rgb to cmyk
    for (int i = 0; i < img.rows; i++) {
        for (int j = 0; j < img.cols; j++) {
            float r = (int)rgb[2].at<uchar>(i, j) / 255.;
            float g = (int)rgb[1].at<uchar>(i, j) / 255.;
            float b = (int)rgb[0].at<uchar>(i, j) / 255.;
            float k = std::min(std::min(1- r, 1- g), 1- b);

            cmyk[0].at<uchar>(i, j) = (1 - r - k) / (1 - k) * 255.;
            cmyk[1].at<uchar>(i, j) = (1 - g - k) / (1 - k) * 255.;
            cmyk[2].at<uchar>(i, j) = (1 - b - k) / (1 - k) * 255.;
            cmyk[3].at<uchar>(i, j) = k * 255.;
        }
    }
}

cv::Mat NumExtractor::prepareImage(cv::Mat img1){
    int min = 200;
    int max = 255;
    cv::Mat img;
    cvtColor( img1, img, CV_BGR2GRAY );
    cv::threshold(img,img, min, max, CV_THRESH_BINARY);
    bitwise_not(img, img);
    return img;
}

bool comp (Rect i,Rect j) { return (i.x<j.x); }

cv::Mat NumExtractor::prepareDataSVM(cv::Mat & other){
    resize(other, other, Size(28,28));
    cv::Size s = other.size();
    int width = (int)s.width;
    int height = (int)s.height;
    cv::Mat ret = other.clone();
      cv::Mat mat(1,width*height, CV_32F);
    for(int i = 0; i < height; ++i)
    {
        for(int j = 0; j < width; ++j)
        {
            mat.at<float>(i*width+j) = (float)other.at<uchar>(i,j);
        }
    }
    mat.reshape(1,1);
    return mat;
}

NumExtractor::NumExtractor(){
        predictor = Algorithm::load<cv::ml::SVM>(svmXMLPath);
    }

cv::Mat NumExtractor::centerImage(cv::Mat img){
    int max;
    if(img.size().width > img.size().height){
            max = img.size().width;
    }
    else{
        max = img.size().height;
    }
    resize(img, img, Size(max, max));
    Size dim = img.size();
    int width = dim.width;
    int height = dim.height;
    int x=width,y=height, xm = 0, ym = 0;

    for(int i = 0; i < width; ++i){
        for(int j = 0; j < height; ++j){
            if(img.at<uchar>(i,j) == 255){
                if(i < x) x = i;
                if(j < y) y = j;
                if(i > xm) xm = i;
                if(j > ym) ym = j;
            }
        }
    }
    int stepx = 0, stepy = 0;
    int b1 = x, b2 = y, b3 = width - xm, b4 = height - ym;
    stepx = (b3 - b1) / 2;
    stepy = (b4 - b2) / 2;
    cv::Mat res(width,height, CV_8U);
    res = cv::Scalar(0,0,0);
    for(int i = x; i < xm; ++i){
        for(int j = y; j < ym; ++j){
            res.at<uchar>(i+stepx, j+stepy) = img.at<uchar>(i,j);
        }
    }
    return res;
}

std::string NumExtractor::extractNumber(std::vector<cv::Mat> digits){
    std::string res{""};
    for(cv::Mat digit : digits){
        cv::Mat temp = prepareImage(digit);
        //copyMakeBorder( temp, temp, 20, 3, 3, 3,BORDER_CONSTANT,Scalar(0,0,0) );
        temp = centerImage(temp);
        int a =(int)predictor->predict(prepareDataSVM(temp));
        res += to_string(a);
    }
    return res;
}

std::pair<std::string, std::string> NumExtractor::readAppIdAndExamId(std::vector<cv::Mat> digits){
    if(digits.size() != 11){
        throw "There is not enough digits for extracting!";
    }
    return std::pair<std::string, std::string>
                (
                    extractNumber(std::vector<cv::Mat>(digits.begin(),digits.begin()+4)),
                    extractNumber(std::vector<cv::Mat>(digits.begin()+4,digits.end()))
                );
}
