#ifndef MATFFORMCONSTANTS_H
#define MATFFORMCONSTANTS_H

#include <cassert>
#include <cmath>
#include <vector>

#define DISPLAY_IMAGE(img) { \
    cv::namedWindow("Debug image window", CV_WINDOW_NORMAL); \
    cv::imshow("Debug image window", img); \
    cv::waitKey(); \
  }

class MatfFormConstants
{
public:
  static constexpr double HEADER_WIDTH = 3997.34;
  static constexpr double HEADER_HEIGHT = 1169.92;

  static const std::vector<unsigned> COLX_DIST;
  static constexpr unsigned COL_SEG_DIST = 4203-2196;

  static const std::vector<unsigned> ROWY_DIST;

  static const std::vector<unsigned> COLX_DELTA;
  static const std::vector<unsigned> ROWY_DELTA;

  static constexpr unsigned APPLICATION_ID_DELTA_X = 1627-501;
  static constexpr unsigned APPLICATION_ID_DELTA_Y = 2755-550;
  static constexpr unsigned EXAM_ID_DELTA_X = 2352-501;
  static constexpr unsigned EXAM_ID_DELTA_Y = 3721-550;

  static constexpr unsigned APPLICATION_ID_BOX_HEIGHT = 2872-2755;
  static constexpr unsigned APPLICATION_ID_BOX_WIDTH = 1743-1627;
  static constexpr unsigned APPLICATION_ID_LINE_WIDTH = 9;
  static constexpr unsigned NUM_OF_APPLICATION_ID_BOXES = 4;

  static constexpr unsigned EXAM_ID_BOX_HEIGHT = 3838-3721;
  static constexpr unsigned EXAM_ID_BOX_WIDTH = 2490-2352;
  static constexpr unsigned EXAM_ID_LINE_WIDTH = 9;
  static constexpr unsigned NUM_OF_EXAM_ID_BOXES = 7;

  static constexpr unsigned JMBG_DELTA_X = 1630-501;
  static constexpr unsigned JMBG_DELTA_Y = 2630-633;
  static constexpr unsigned JMBG_BOX_WIDTH = 1747-1630;
  static constexpr unsigned JMBG_BOX_HEIGHT = 2748-2630;
  static constexpr unsigned JMBG_LINE_WIDTH = 9;
  static constexpr unsigned NUM_OF_JMBG_BOXES = 13;
};

#endif // MATFFORMCONSTANTS_H
