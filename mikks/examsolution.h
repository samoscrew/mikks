#ifndef EXAMSOLUTION_H
#define EXAMSOLUTION_H

#include "circledetector.h"

#include <QObject>
#include <QString>

#include <vector>

#include <opencv2/core/core.hpp>

class ExamSolution : public QObject
{
  Q_OBJECT
public:
  static constexpr char INVALID_ANSWER = 'X';
  static constexpr unsigned INVALID_ROW = 10;
  static constexpr unsigned INVALID_TASK_NUM = 21;
  static const std::vector<char> COLUMN_LETTER_MAP;

  explicit ExamSolution(const cv::Mat &img, double scale, QObject *parent = 0);

  QString operator()();


private:
  unsigned findRow(const cv::Vec3f& c) const;
  unsigned findTaskNumber(const cv::Vec3f& c) const;
  char findWhichAnswer(const cv::Vec3f& c) const;

  void drawNet();

  CircleDetector cd_;
  double scale_;
};

#endif // EXAMSOLUTION_H
