#ifndef CIRCLEDETECTOR_H
#define CIRCLEDETECTOR_H

// Project headers
#include "configuration.h"

// Qt headers
#include <QObject>
#include <QString>

// OpenCV headers
#include <opencv2/core/core.hpp>

// Forward declaration
class QString;

class CircleDetector : public QObject
{
  Q_OBJECT
public:
  // Construct img from path
  explicit CircleDetector(const cv::Mat& img, QObject *parent = 0);

  // Detect circle and return centers
  std::vector<cv::Vec3f> detect();

  inline const cv::Mat& image() const { return img_; }
private:
  cv::Mat img_;
};

#endif // CIRCLEDETECTOR_H
