Hi people! :)

PREPARATION:

So to cut it short, software needed for project:
1) Qt 5.4
2) OpenCV 3.0
3) LAMP stack (Linux Apache MySQL PHP)

Qt can be downloaded from their web site: https://www.qt.io/download-open-source/ . It can be installed easily using GUI installer.

OpenCV can be downloaded from: http://opencv.org/ (most right link OpenCV for Linux/Mac) . OpenCV can be installed using cmake.
In case you don't have cmake installed type: sudo apt-get install cmake . Next, unzip downloaded OpenCV to some location of your choise.
After that, navigate yourself to that location and create a new directory: mkdir build && cd build. From this new directory issue a command:
cmake ../ , and if everything goes well you now have build files written to current directory. If not, consult this page for missing
dependecies: http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html#linux-installation . With build files in place
run: sudo make install -j4 (-j4 means use 4 cores for compilation, if you have slower machine put lower number). It installs for couple of minutes.

LAMP stack can be installed as described here: https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-14-04

GIT:
So in an odd case that someone doesn't have git installed: sudo apt-get install git is a way to go.
Next pick a location on your machine and create a new directory in that location (I called it 'mikks').
Few more commands and you are on a master branch:
cd mikks // or whatever directory name is
git init
git remote add origin https://zbetmen@bitbucket.org/samoscrew/mikks.git
git pull

Qt Creator instructions:
When importing project do not accept *.user.pro file to be imported cause that can mess up your build.

Server part of project:
We should put web part of project in the same directory where we've initialized git (parent directory for both web and C++ part).
