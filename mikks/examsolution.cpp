#include "examsolution.h"
#include "matfformconstants.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

constexpr char ExamSolution::INVALID_ANSWER;
const vector<char> ExamSolution::COLUMN_LETTER_MAP = { 'A', 'B', 'C', 'D', 'E', 'N' };

ExamSolution::ExamSolution(const Mat& img, double scale, QObject *parent)
  : QObject(parent), cd_(img), scale_(scale)
{
}

QString ExamSolution::operator()()
{
  QString answers(20, INVALID_ANSWER);
  vector<Vec3f> circles = move(cd_.detect());
  for (const auto& c : circles)
  {
    // If we have more than 2 circled answers for same task that equals invalid answer
    unsigned taskNum = findTaskNumber(c) - 1;
    if (answers[taskNum] != INVALID_ANSWER)
    {
      answers[taskNum] = INVALID_ANSWER;
    }
    else { // We put answer in place
      answers[taskNum] = findWhichAnswer(c);
    }
  }

  return move(answers);
}

unsigned ExamSolution::findRow(const Vec3f &c) const
{
  // Find in which row is circle detected
  int y = MatfFormConstants::ROWY_DELTA[1]*scale_;
  for (unsigned i = 0; i < 9; ++i)
  {
    if (y > c.val[1])
      return i;

    y += MatfFormConstants::ROWY_DELTA[i+2]*scale_;
  }
  return 9;
}

unsigned ExamSolution::findTaskNumber(const Vec3f &c) const
{
  // Check that circle is in one of rows
  unsigned row = findRow(c);
  if (row == INVALID_ROW)
    return INVALID_TASK_NUM;

  // Return task number according to the row and coordinate of the circle
  return c.val[0] < cd_.image().cols/2 ? row+1 : row+11;
}

char ExamSolution::findWhichAnswer(const Vec3f &c) const
{
  // Determine delta factor (in which segment is circle)
  unsigned delta = c.val[0] < cd_.image().cols/2 ? 0 : MatfFormConstants::COL_SEG_DIST * scale_;

  // Find in which column is circle detected
  int x = MatfFormConstants::COLX_DELTA[1]*scale_ + delta;
  for (unsigned i = 0; i < 5; ++i)
  {
    if (x > c.val[0])
      return COLUMN_LETTER_MAP[i];
    x += MatfFormConstants::COLX_DELTA[i+2]*scale_;
  }

  return COLUMN_LETTER_MAP[5];
}

void ExamSolution::drawNet()
{
  auto img = cd_.image();
  double ycoord = 0.0;
  for (unsigned i = 0; i < MatfFormConstants::ROWY_DELTA.size(); ++i)
  {
    ycoord += MatfFormConstants::ROWY_DELTA[i] * scale_;
    line(img, {0, ycoord}, {img.cols, ycoord}, {255, 0, 0}, 4, 8);
  }

  double xcoord = 0.0;
  for (unsigned i = 0; i < MatfFormConstants::COLX_DELTA.size(); ++i)
  {
    xcoord += MatfFormConstants::COLX_DELTA[i] * scale_;
    line(img, {xcoord, 0}, {xcoord, img.rows}, {255, 0, 0}, 4, 8);
  }

  xcoord = MatfFormConstants::COL_SEG_DIST * scale_;
  for (unsigned i = 0; i < MatfFormConstants::COLX_DIST.size(); ++i)
  {
    xcoord += MatfFormConstants::COLX_DELTA[i] * scale_;
    line(img, {xcoord, 0}, {xcoord, img.rows}, {255, 0, 0}, 4, 8);
  }

  DISPLAY_IMAGE(img)
}
